class Machine {
  constructor(){
    this._isTurn = false;
  }
  turnOn(){
    this._isTurn = true;
    console.log("Вкл")
  };

  turnOff() {
    this._isTurn = false;
    console.log('Выкл')
  };
}

class HomeAppliance extends Machine {
  constructor(){
    super();
    this._pluget = false;
  }


  plugIn() {
    this._pluget = true;
    console.log("Подключено в сеть")
  };

  plugOff() {
    this._pluget = false;
    console.log("Не влючено в сеть");
  };
}

class WashingMachine extends HomeAppliance {
  constructor(){
    super();
    this._pluget = false;
  }

  run() {
    if (this._pluget === false || this._isTurn === false){
      console.log('Не подлючено к сети');
    }else {
      console.log('Машина начила стирку');
    }
  };

}

class LightSource extends HomeAppliance{
  constructor(){
    super();
    this.level = 0;
  }


  setLevel(lvl) {
    if (this._pluget === false){
      console.log("Подруби питание")
    }else {

      if (lvl < 1 || lvl > 100){
        console.log('Не правельный уровень света');
        this.turnOff();
      } else{
        this.level = lvl;
        console.log('Уровень света нормальная', this.level);
        this.turnOn()
      }
    }

  };
}
class AutoVehicle extends Machine{
  constructor(){
    super();
    this.x = 0;
    this.y = 0;
  }

  setPosition(a, b) {
    this.x = a;
    this.y = b;
  };
}

class Car extends AutoVehicle{
  constructor(){
    super();
    this.speed = 10;
  }




  setSpeed(speed) {
    this.speed = speed;
  };
  run(x, y) {
    const interval = setInterval(() => {
      let newX = this.x + this.speed;
      if(newX >= x) {
        newX = x
      }
      let newY = this.y + this.speed;
      if (newY >= y){
        newY = y;
      }

      this.setPosition(newX, newY);
      console.log('Машина двигается по кардинатам ' + newX + ' ' + newY);
      if (newX === x && newY === y){
        clearInterval(interval);
        console.log('Машина на месте')
      }
    }, 1000);
  }
}
